FROM ubuntu:14.04

# build command behind proxy
# docker build --build-arg http_proxy=http://proxy.hs-owl.de:80 --build-arg https_proxy=http://proxy.hs-owl.de:80 -t assos_listen .


ENV PYTHONWARNINGS="ignore:a true SSLContext object"
WORKDIR /usr/src/app

# Install dependencies
COPY requirements.txt ./
COPY code .
ENV PYTHONWARNINGS="ignore:a true SSLContext object"

# Install dependencies
RUN apt-get update && apt-get install -y \
 wget \
 curl \
 git \
 swig \
 libgtk-3-dev \
 software-properties-common \
 build-essential \
 zip \
 unzip \
 sox \
 libsox-fmt-mp3 \
 libimage-exiftool-perl \
 python2.7 \
 python-pip \
 python-dev \
 libffi-dev \
 libssl-dev \
 ipython \
 ipython-notebook \
 python-matplotlib \
 libfreetype6-dev \
 libxft-dev \
 libblas-dev \
 liblapack-dev \
 libatlas-base-dev \
 gfortran \
 libpulse-dev \
 aubio-tools \
 libaubio-dev \
 libaubio-doc \
 libyaml-dev \
 libfftw3-dev \
 libavcodec-dev \
 libavformat-dev \
 libavutil-dev \
 libavresample-dev \
 libsamplerate0-dev \
 libtag1-dev \
 python-numpy-dev \
 python-numpy \
 python-yaml \
 cmake \
 libjack-dev \
 libasound2-dev \
 libsndfile1-dev \
 praat \
 && python -m pip install -U pip \
 && pip install -U \
 setuptools \
 pyOpenSSL \
 ndg-httpsclient \
 pyasn1 \
 requests \
 unicodecsv \
 youtube-dl \
 six \
 pydub \
 numpy \
 jupyter \
 pandas \
 matplotlib \
 scipy \
 sklearn \
 librosa \
 aubio \
 moviepy \
 pyAudioAnalysis \
 pocketsphinx \
 speechrecognition \
 tornado \
 pathlib \
 tflearn \
 scikits.talkbox \
 scikits.audiolab \
 git+git://github.com/hipstas/audio-tagging-toolkit.git \
 && git clone https://github.com/MTG/essentia.git \
 && cd essentia \
 && ./waf configure --mode=release --build-static --with-python --with-cpptests --with-examples --with-vamp \
 && ./waf \
 && ./waf install \
 && cd ../ \
 && rm -rf essentia

# Install FFmpeg with mp3 support
RUN add-apt-repository -y ppa:mc3man/trusty-media \
 && apt-get update -y \
&& apt-get install -y ffmpeg gstreamer0.10-ffmpeg

# Install custom requirements from file
# RUN pip install --no-cache-dir -r requirements.txt

# Install paura dependencies
RUN apt-get update && apt-get install -y python-alsaaudio
RUN apt-get update && apt-get install -y  python-opencv

# Configure container startup
ENV SHELL /bin/bash
WORKDIR /home/sharedfolder

# get paura
CMD cd /home/sharedfolder/
RUN git clone https://github.com/gannebamm/paura.git

#CMD cd /home/sharedfolder/ && wget -nc https://github.com/hipstas/audio-tagging-toolkit/blob/master/scripts/Classify_and_Play.zip?raw=true -O Classify_and_Play.zip
CMD jupyter notebook --ip 0.0.0.0 --no-browser --allow-root --NotebookApp.iopub_data_rate_limit=1.0e10 --NotebookApp.token=''

# example to run this container
# docker run -it --name assos_listen_container -p 8888:8888 -v C:\Users\Florian\Documents\assos_listen\:/home/sharedfolder/ assos_listen